#!/usr/bin/env python
#
# Create an encrypted CRAMFS image on a loop device.
#

import contextlib
import os
import optparse
import shutil
import subprocess
import sys
import tempfile


_required_executables = [
    '/sbin/losetup',
    'cryptsetup',
    'mksquashfs',
]

def _find_in_path(binary):
    def _is_executable(path):
        return os.path.isfile(path) and os.access(path, os.X_OK)
    if binary.startswith('/'):
        return _is_executable(binary)
    for path in os.environ['PATH'].split(os.pathsep):
        testp = os.path.join(path, binary)
        if _is_executable(testp):
            return testp

def check_required_executables():
    for binary in _required_executables:
        if not _find_in_path(binary):
            raise Exception('Missing "%s" binary' % binary)


def _find_loop_device():
    output = subprocess.check_output(['/sbin/losetup', '-f'])
    return output.strip()


@contextlib.contextmanager
def loop_device(loopfile, target_size):
    # LUKS header size for a 256-bit key and align-offset=8.
    header_size = 2056 * 512
    # Round device size up to the nearest 'block_size'.
    block_size = 64 * 1024
    n_blocks = (header_size + target_size + block_size - 1) / block_size
    loop_size = n_blocks * block_size
    # Create an empty file of the required size.
    subprocess.check_call(['dd', 'if=/dev/zero', 'of=%s' % loopfile,
                           'bs=%d' % block_size, 'count=%d' % n_blocks,
                           'status=none'])
    # Mount a new loop device on it.
    loopdev = _find_loop_device()
    subprocess.check_call(['/sbin/losetup', loopdev, loopfile])
    try:
        yield loopdev
    finally:
        # Cleanup
        subprocess.check_call(['/sbin/losetup', '-d', loopdev])


@contextlib.contextmanager
def dm_setup(loopdev, cipher, key_size):
    cryptdev = 'mkimage_%d' % os.getpid()
    # This results in a LUKS offset of 1MB (so the device has to be
    # at least that large).
    subprocess.check_call(
        ['cryptsetup', 'luksFormat', '--align-payload=8',
         '--key-size=%d' % key_size, '--cipher=%s' % cipher, loopdev])
    subprocess.check_call(
        ['cryptsetup', 'luksOpen', loopdev, cryptdev])
    try:
        yield '/dev/mapper/' + cryptdev
    finally:
        subprocess.check_call(
            ['cryptsetup', 'luksClose', cryptdev])


@contextlib.contextmanager
def mkfs(source_dir):
    tmp_fd, imagefile = tempfile.mkstemp(prefix='mkimage_')
    os.close(tmp_fd)
    os.remove(imagefile)

    try:
        subprocess.check_call(['mksquashfs', source_dir, imagefile])
        yield imagefile
    finally:
        try:
            os.remove(imagefile)
        except:
            pass


def dd(src_path, dst_path):
    with open(src_path, 'r') as rfd, open(dst_path, 'w') as wfd:
        shutil.copyfileobj(rfd, wfd)
    

def mkimage(source_dir, output_file, cipher, key_size, compress):
    # 1. Create the squashfs image on a temporary file.
    # 2. Setup the encrypted loop device.
    # 3. Copy the image data onto the loop device.
    loopfd, loop_file = tempfile.mkstemp(
        prefix='.loop_', dir=os.path.dirname(output_file))
    os.close(loopfd)

    try:
        with mkfs(source_dir) as imagefile:
            target_size = os.path.getsize(imagefile)
            with loop_device(loop_file, target_size) as loopdev:
                with dm_setup(loopdev, cipher, key_size) as cryptdev:
                    dd(imagefile, cryptdev)
            if compress:
                with open(output_file, 'w') as fd:
                    subprocess.check_call([compress, '-9', '-c', loop_file],
                                          stdout=fd)
            else:
                os.rename(loop_file, output_file)
    finally:
        try:
            os.remove(loop_file)
        except:
            pass


def main():
    parser = optparse.OptionParser(usage='''%prog [<OPTIONS>] <PATH>

Create an encrypted read-only filesystem image using dm-crypt and
squashfs. The resulting image is protected by a passphrase and is
compressed.

The tool will interactively ask for a passphrase 3 times.
''')
    parser.add_option('-o', '--output', dest='output', default='output.img',
                      help='location of the resulting encrypted image (default %default)')
    parser.add_option('--key-size', dest='key_size', type='int', default=256,
                      help='set the dm-crypt key size (default %default bits)')
    parser.add_option('--cipher', dest='cipher', default='aes-xts-plain64',
                      help='set the dm-crypt cipher (default %default)')
    parser.add_option('-z', dest='compress', action='store_const', const='gzip',
                      help='compress image using gzip')
    parser.add_option('-j', dest='compress', action='store_const', const='bzip2',
                      help='compress image using bzip2')
    parser.add_option('-x', dest='compress', action='store_const', const='xz',
                      help='compress image using xz')
    opts, args = parser.parse_args()
    if len(args) != 1:
        parser.error('Wrong number of arguments')
        
    if os.getuid() != 0:
        print >>sys.stderr, 'ERROR: must be running as root!'
        return 1

    output_file = opts.output
    if opts.compress == 'gzip' and not output_file.endswith('.gz'):
        output_file += '.gz'
    elif opts.compress == 'bzip2' and not output_file.endswith('.bz2'):
        output_file += '.bz2'
    elif opts.compress == 'xz' and not output_file.endswith('.xz'):
        output_file += '.xz'
        
    try:
        check_required_executables()
        mkimage(args[0], output_file, opts.cipher, opts.key_size, opts.compress)
        return 0
    except Exception as e:
        import traceback
        traceback.print_exc()
        print >>sys.stderr, 'ERROR: %s' % str(e)
        return 1


if __name__ == '__main__':
    sys.exit(main())
