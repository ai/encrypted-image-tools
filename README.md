encrypted-image-tools
=====================

A simple suite of tools to create and update encrypted LVM images
(password-protected) containing sensitive credentials.

To install, simply run:

    $ sh autogen.sh
    $ ./configure
    $ sudo make install

